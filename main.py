# @author Leon

import pandas as pd
import numpy as np
from hybrid import General, BasketRecommender, Hybrid
from IICF import IICF
from UUCF import UUCF

# Define objects from classes
g = General()
uucf = UUCF()
iicf = IICF()
br = BasketRecommender()
hybrid = Hybrid()

# Load data files
ratings = pd.read_csv('./ml-latest-small/ratings.csv', engine='python')
movies = pd.read_csv('./ml-latest-small/movies.csv', engine='python')

# General
index = g.get_dict_index(movies)
all_ratings = g.get_ratings_all_movies(ratings, movies)
average_ratings = g.get_average_r_movies(ratings, movies)
item_matrix = iicf.calculate_matrix(ratings, movies, average_ratings, all_ratings, index)