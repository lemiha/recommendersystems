# @author Leon

import pandas as pd
import numpy as np

# Item-Item Collaborative Filtering
class IICF:
    # Calculate the all the similarites given a movie
    def calculate_item(self, movie1, ratings, movies, average_ratings, all_ratings, excluded, index_m):
        # Users that rated the movie
        users_movie1 = ratings.loc[ratings['movieId'].isin([movie1])]

        if len(users_movie1) == 0:
            return 0

        # Loop to calculate the similarities
        records = []
        for user in users_movie1['userId'].values:
            # For each user that rated movie1, gather the ratings made by each user
            i_ratings = ratings.loc[ratings['userId'].isin([user])]

            # Rating the user made for movie1
            movie1_rating = i_ratings.loc[i_ratings['movieId'].isin([movie1])].values[0]
            
            # All the ratings excluding movie1
            other_ratings = i_ratings[i_ratings['movieId'] != movie1]

            # Make an array for calculating the predictiosn
            for movie2 in other_ratings.values:
                tmp = [user, int(movie1_rating[1]), int(movie2[1]), movie1_rating[2], movie2[2]]
                records.append(tmp)

        # Since the matrix is symetric, the already processed movies from movie1 array are excluded
        records = np.array(records)
        movies_tmp = records[:,2]
        to_delete = np.where(np.in1d(movies_tmp,excluded))
        movies_tmp = np.delete(movies_tmp, to_delete)

        # Get hte average for the first movie
        average_movie1 = np.where(average_ratings[:,0] == movie1)
        average_movie1 = average_ratings[average_movie1,1][0][0]
        
        # For all the movies (that are not excluded), I calculate the similarities
        sim = []
        for movie2 in movies_tmp:
            index = np.where(records[:,2] == movie2)
            all_user_ratings_m1 = all_ratings[index_m[movie1]]
            all_user_ratings_m2 = all_ratings[index_m[movie2]]

            average_movie2 = np.where(average_ratings[:,0] == movie2)
            average_movie2 = average_ratings[average_movie2,1][0][0]

            numerator = np.multiply(records[:,3][index] - average_movie1,records[:,4][index] - average_movie2)
            numerator = np.sum(numerator)

            denominator = np.sqrt(np.sum(np.square(all_user_ratings_m1 - average_movie1))) * np.sqrt(np.sum(np.square(all_user_ratings_m2 - average_movie2)))

            if denominator == 0:
                sim.append([movie1, int(movie2), 0])
            else:
                sim.append([movie1, int(movie2), numerator/denominator])

        return sim
    
    # Calculate the whole matrix based 
    def calculate_matrix(self, ratings, movies, average_ratings, all_ratings, index_m):
        movieids = movies['movieId'].values
        size_of_movies = len(movieids)
        matrix = np.empty((size_of_movies,size_of_movies,))
        matrix[:] = np.nan

        index = {}
        for i in range(size_of_movies):
            index[movieids[i]] = i

        excluded = []
        count = 0
        for movie1 in movieids:  
            tmp = self.calculate_item(movie1, ratings, movies, average_ratings, all_ratings, excluded, index_m)

            if tmp == 0:
                continue
            else:
                for e in tmp:
                    im = e[1]
                    sim = e[2]

                    if sim != 0:
                        matrix[index[im],index[movie1]] = sim
            
            excluded.append(movie1)
            count += 1

        tmp = pd.DataFrame(data=matrix, index=movieids, columns=movieids)
        return tmp
    
    # Get the similaries for movies the user has rated
    def get_similarities(self, userid, itemid, item_matrix, index, ratings, movies, positive=True, negative=True):        
        # Get the movies rated by the user
        user_movies_pd = ratings.loc[ratings['userId'].isin([userid])]
        user_movies = user_movies_pd['movieId'].values

        # Get the cosine similarity scores from the user
        row_index = index[itemid]
        row = item_matrix.iloc[row_index].values[1:]
        col = item_matrix[str(itemid)].values

        # Exclude nan-values
        row_new = []
        col_new = []
        movieids = movies['movieId'].values
        movie_titles = movies['title'].values
        for i in range(len(movieids)):
            if int(movieids[i]) in user_movies:
                tmp_rating = user_movies_pd.loc[user_movies_pd['movieId'].isin([movieids[i]])]
                tmp_rating = tmp_rating['rating'].values[0]
 
                if np.isnan(row[i]) == False:
                    row_new.append([movieids[i], movie_titles[i], row[i], tmp_rating])
 
                if np.isnan(col[i]) == False:
                    col_new.append([movieids[i], movie_titles[i], col[i], tmp_rating])
 
        row_new = np.array(row_new)
        col_new = np.array(col_new)
 
        # Combine row and coloumn
        if len(row_new) == 0:
            combined = col_new
        elif len(col_new) == 0:
            combined = row_new
        else:
            combined = np.concatenate((row_new,col_new))
         
        # Return the similarities
        if len(combined) == 0:
            return combined            
        elif negative == True and positive == True:
            return combined
        elif negative == True and positive == False:
            return combined[combined[:,2].astype(float) < 0]
        else:
            return combined[combined[:,2].astype(float) > 0]

        
    # Simple method to count the number of similarities
    def count_similarities(self, userid, itemid, item_matrix, index, ratings, movies, positive=True, negative=True):
        tmp = self.get_similarities(userid, itemid, item_matrix, index, ratings, movies, positive=True, negative=True)
        return len(tmp)
    
    # Methods to find the k most similar items
    def top_k_sim(self, userid, itemid, item_matrix, index, movies, ratings, k, toprint=False):
        # Get similarities
        sim_array = self.get_similarities(userid, itemid, item_matrix, index, ratings, movies, negative=False)

        # Sort the similarities by decending order
        if len(sim_array) != 0:
            ind = np.argsort(-sim_array[:,2].astype(float)) 
            sim_array = sim_array[ind]

        # Return either array or pandas dataframe
        if len(sim_array) < k:
            if toprint == True:
                return pd.DataFrame(data=sim_array, index=list(range(len(sim_array))), columns=['movieId', 'title', 'Sim Score', 'Rating'])
            else:
                return sim_array[0:len(sim_array)]
        else:
            if toprint == True:
                return pd.DataFrame(data=sim_array[0:k], index=list(range(len(sim_array[0:k]))), columns=['movieId', 'title', 'Sim Score', 'Rating'])
            else:
                return sim_array[0:k]

    # Method for predicting the rating for item-item
    def rating_ii_prediction(self, userid, itemid, item_matrix, index, movies, ratings, k):
        sim_array = self.top_k_sim(userid, itemid, item_matrix, index, movies, ratings, k)

        if len(sim_array) == 0:
            return 0
        
        sim_values = sim_array[:,2].astype(float)
        rating = sim_array[:,3].astype(float)

        numerator = np.sum( np.multiply(sim_values, rating) )
        denominator = np.sum( np.abs(sim_values) )

        if denominator == 0:
            return 0
        else:
            return numerator/denominator

    # Method to get the Top-n recommendations 
    def top_n_recommendations(self, userid, item_matrix, index, movies, ratings, k, topn, toprint=False):
        # Get all the movies not rated by the user
        user_movies = ratings.loc[ratings['userId'].isin([userid])]
        user_movies = user_movies['movieId'].values
        movieids = movies.loc[~movies['movieId'].isin(user_movies)]
        movie_titles = movieids['title'].values
        movieids = movieids['movieId'].values

        # Calculate all the rating predictions for each movie
        reccomendations = []
        for i in range(len(movieids)):
            rating = self.rating_ii_prediction(userid, movieids[i], item_matrix, index, movies, ratings, k)
            reccomendations.append([movieids[i], movie_titles[i], rating])
        
        reccomendations = np.array(reccomendations)
        
        # Sort by decending order
        if len(reccomendations) != 0:
            ind = np.argsort(-reccomendations[:,2].astype(float)) 
            reccomendations = reccomendations[ind]

        # Return the recommendations
        if len(reccomendations) < k or topn == None:
            tmp = pd.DataFrame(data=reccomendations, index=list(range(len(reccomendations))), columns=['movieId', 'title', 'R. Score'])
            if toprint == True:
                tmp.to_csv(str(userid) + '_ii_topn.csv')
            else:
                return tmp
        else:
            tmp = pd.DataFrame(data=reccomendations[0:topn], index=list(range(len(reccomendations[0:topn]))), columns=['movieId', 'title', 'R. Score'])
            if toprint == True:
                tmp.to_csv(str(userid) + '_ii_topn.csv')
            else:
                return tmp[0:topn]
            
    # Methods to find a single value in the cosine similarity matrix
    def find_cos_similarity(self, item1, item2, item_matrix, index):
        highest = max(item1, item2)
        lowest = min(item1, item2)
        row_index = index[highest]
        row = item_matrix.iloc[row_index]
        sim = row[str(lowest)]

        return sim