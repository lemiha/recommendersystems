# @author Leon

import pandas as pd
import numpy as np

# Class for the User-User Collaborative Filtering
class UUCF:
    # Calculate the peatson correlation given two users
    def pearson_correlation(self, userid1, userid2, ratings, weighting):
        # Delta used for the scaling of the pearson correlation
        delta = 10.0

        # Find the movies the users has in common
        user_movies = ratings.loc[ratings['userId'].isin([userid1])]
        user2_movies = ratings.loc[ratings['userId'].isin([userid2])]
        tmp1 = user_movies.loc[user_movies['movieId'].isin(user2_movies['movieId'])]
        tmp2 = user2_movies.loc[user2_movies['movieId'].isin(user_movies['movieId'])]

        # Gather the ratings for both the distinct users
        ra = tmp1['rating'].values
        ru = tmp2['rating'].values

        # Handle special cases
        if len(tmp1) == 1 or len(tmp1) == 0:
            return 0
        
        # Calculate the correlation
        tmp1 = ra - np.mean(ra)
        tmp2 = ru - np.mean(ru)

        numerator = np.sum(np.multiply(tmp1,tmp2))
        denominator = np.sqrt(np.sum(tmp1**2)) * np.sqrt(np.sum(tmp2**2))

        if denominator == 0:
            return 0
        
        tmp = (numerator/denominator)
        if tmp < 0:
            return 0
        else:
            if weighting == True:
                return tmp*(min(delta,len(ra))/delta)
            else:
                return tmp

    # Find the top-k neighnours given a userid and itemid
    def find_neighbours(self, userid, itemid, ratings, k):
        # Ratings of the movie, excluding the movies the user rated
        users_w_movie = np.array(ratings.loc[ratings['movieId'].isin([itemid])]['userId'])
        users_w_movie = np.delete(users_w_movie, np.where(users_w_movie == userid), axis=0)

        # Get all the pearson correlations of the movie
        tmp = []
        for idd in users_w_movie:
            tmp.append([idd, self.pearson_correlation(userid, idd, ratings, True)])
        tmp = np.array(tmp)

        # Handle special cases
        if len(tmp) == 0:
            return []
        index = np.where(tmp[:,1] > 0)
        if len(index) == 0:
            return []

        # Make to pandas dataframe
        neighbours = pd.DataFrame({'User ID':tmp[:,0],'S. Value':tmp[:,1]})
        neighbours['User ID'] = neighbours['User ID'].astype(int)

        # Return the matrix
        if k == None:
            return neighbours.sort_values(by=['S. Value'], ascending=False)
        else:
            return neighbours.sort_values(by=['S. Value'], ascending=False)[0:k]


    # Calculate the deviation from the mean
    def deviation_from_mean(self, target_id, itemid, ratings, k):
        # Gather the neighbours
        neighbours = self.find_neighbours(target_id, itemid, ratings, k)
        
        # Special case
        if len(neighbours) == 0:
            return 0

        userids = np.array(neighbours)

        # Calculate the deviation from the mean
        numerator = 0
        denominator = 0
        for idd in userids:
            user = int(idd[0])

            # Gather all the ratings made by the user and the ratings by the user on the movie
            tmp = ratings.loc[ratings['userId'].isin([user])]
            user_all_rating = tmp['rating']
            user_item_rating = tmp.loc[ratings['movieId'].isin([itemid])]['rating'].values[0]

            # Calculate pearson correlation and numerator and denominator
            tmp = self.pearson_correlation(target_id, user, ratings, True)
            mean_user = np.mean(user_all_rating)
            numerator += (user_item_rating - mean_user)*tmp
            denominator += tmp
        
        if denominator == 0:
            return 0
        else:
            return numerator/denominator

    # Get the rating prediction
    def rating_uu_prediction(self, userid, itemid, ratings):
        user_movies = ratings.loc[ratings['userId'].isin([userid])]
        ra = user_movies['rating'].values
        dm = self.deviation_from_mean(userid, itemid, ratings, 20)


        return np.mean(ra)+dm
    
    
    def top_recommendations(self, userid, movies, ratings, toprint = False):
        # Get all the movies not rated by the user
        user_movies = ratings.loc[ratings['userId'].isin([userid])]
        user_movies = user_movies['movieId'].values
        movie_ids = movies.loc[~movies['movieId'].isin(user_movies)]
        movie_ids = movies['movieId'].values

        # Get prediction for all the movies in movie_ids
        count = 0
        p_ratings = []
        for movieid in movie_ids:
            count += 1
            if count % 10 == 0:
                print(count)

            p_ratings.append(self.rating_uu_prediction(userid, movieid, ratings))

        # Save to dataframe
        new = pd.DataFrame([movies['movieId'],movies['title']]).transpose()
        new['R. Score'] = p_ratings

        if toprint == False:
            return new
        else:
            f_name = 'uu_for_user_' + str(userid) + '.csv'
            new.to_csv(f_name, sep='\t')