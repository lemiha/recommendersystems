# @author Leon

import pandas as pd
import numpy as np
from IICF import IICF
from UUCF import UUCF
import os

# A class for general methods
class General:
    # Get the title from a movie
    def get_title(self, movieid, movies):
        movie_title = movies.loc[movies['movieId'].isin([movieid])]

        return movie_title

    # Get the average from all the movies
    def get_average_r_movies(self, ratings, movies):
        movies_array = movies['movieId'].values

        averages_ratings = []
        for movie in movies_array:
            ratings_for_movie = ratings.loc[ratings['movieId'].isin([movie])]
            ratings_for_movie = ratings_for_movie['rating'].values

            ar = np.mean(ratings_for_movie)

            averages_ratings.append([movie, ar])

        tmp = np.array(averages_ratings)

        return tmp
    
    # Make a dictionary to relate a mapping to a index to the movieids
    def get_dict_index(self, movies):
        movieids = movies['movieId'].values
        size_of_movies = len(movieids)

        index = {}
        for i in range(size_of_movies):
            index[movieids[i]] = i
            
        return index

    # Get ratings for each individual movies
    def get_ratings_all_movies(self, ratings, movies):
        movieids = movies['movieId'].values

        ratings_movies = []
        for movie in movieids:
            all_user_ratings_m = ratings.loc[ratings['movieId'].isin([movie])]
            all_user_ratings_m = all_user_ratings_m['rating'].values
            
            ratings_movies.append(all_user_ratings_m)

        return ratings_movies

    # Get the positive number of similarities for the whole matrix
    def get_positive_count(self, item_matrix):
        nr_of_col = len(item_matrix.columns.values)

        total_sum = 0
        for i in range(1,nr_of_col):
            tmp = item_matrix[item_matrix.columns[i]].values
            tmp = tmp[~np.isnan(tmp)]
            total_sum += np.sum(np.array(tmp) > 0)

        return total_sum

# The basket recommender
class BasketRecommender:

    # Get all the similarites for an item
    def get_item_similarities(self, itemid, item_matrix, index, movies, negative, positive=True):
        # Get the corresponding row an coloumn
        row_index = index[itemid]
        row = item_matrix.iloc[row_index].values[1:row_index+1]
        col = item_matrix[str(itemid)].values[row_index:]
        
        # Combine row and coloumn
        if len(row) == 0:
            tmp_sim = col
        elif len(col) == 0:
            tmp_sim = row
        else:
            tmp_sim = np.concatenate([row,col])            
        
        # Replace nan with zeros
        for i in range(len(tmp_sim)):
            if np.isnan(tmp_sim[i]):
                tmp_sim[i] = 0

            if negative == False:
                if tmp_sim[i] < 0:
                    tmp_sim[i] = 0
            
        return tmp_sim

    # Calculate the top n basket recommendations
    def top_n_br_recommendations(self, basket, item_matrix, index, movies, topn, negative, positive=True, toprint=False):

        # Get similarities
        if len(basket) == 1:
            # If only one item in basket, then return top n similarities in basket
            sim_total = self.get_item_similarities(basket[0], item_matrix, index, movies, negative)
        else:
            # If multiple items in basket, calculate the sum over all similarities elementwise
            sim_all = []
            for itemid in basket:
                tmp = self.get_item_similarities(itemid, item_matrix, index, movies, negative)
                sim_all.append(tmp)

            sim_total = np.sum(sim_all, axis = 0)

        # Add movie id and title to the similarity score
        movieids = movies['movieId'].values
        movies_titles = movies['title'].values
        sim_new = []
        for i in range(len(movieids)):
            sim_new.append([movieids[i], movies_titles[i], sim_total[i]])
        sim_new = np.array(sim_new)

        # Sort by decending value
        ind = np.argsort(-sim_new[:,2].astype(float)) 
        sim_new = sim_new[ind]

        # Return 
        if negative == True and positive == True:
            sim_new = sim_new
        elif negative == True and positive == False:
            sim_new = sim_new[sim_new[:,2].astype(float) < 0]
        else:
            sim_new = sim_new[sim_new[:,2].astype(float) > 0]
        
        tmp = pd.DataFrame(data=sim_new, index=list(range(len(sim_new[:,0]))), columns=['movieId', 'title', 'S. Score'])
        return tmp[0:topn]
        

class Hybrid(UUCF, IICF):
    def top_hybrid_recommendations(self, userid, item_matrix, index, movies, ratings, topn, toprint_in):
        # Recommendation scores for UUCF and IICF respectively
        uucf_recommendations = UUCF.top_recommendations(self, userid, movies, ratings, toprint = toprint_in)
        iicf_recommendations = IICF.top_n_recommendations(self, userid, item_matrix, index, movies, ratings, 20, None, toprint = toprint_in)

        # Scale every similarity value by the wieght 0.5
        uucf_recommendations['R. Score'] = uucf_recommendations['R. Score']*0.5 
        iicf_recommendations['R. Score'] = iicf_recommendations['R. Score']*0.5

        # Sort by movieid
        uucf_recommendations = uucf_recommendations.sort_values(by='movieId', ascending=False)
        iicf_recommendations = iicf_recommendations.sort_values(by='movieId', ascending=False)

        # Find intersection of recommendations
        uu_v = uucf_recommendations['movieId'].values.astype(int)
        ii_v = iicf_recommendations['movieId'].values.astype(int)
        common_values = np.intersect1d(uu_v, ii_v)
        uucf_recommendations = uucf_recommendations.loc[uucf_recommendations['movieId'].isin(common_values)]
        iicf_recommendations = iicf_recommendations.loc[iicf_recommendations['movieId'].isin(common_values)] 

        # Sum over all ratings to get the hybrid recommendation
        new_rating = np.sum([uucf_recommendations['R. Score'].values, iicf_recommendations['R. Score'].values], axis = 0)

        # Sort again by decending value
        new_r = uucf_recommendations
        new_r['R. Score'] = new_rating
        new_r = new_r.sort_values(by='R. Score', ascending=False)

        return new_r[0:topn]     